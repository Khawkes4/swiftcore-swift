//===----------------------------------------------------------------------===//
//
// This source file is part of the SwiftNIO open source project
//
// Copyright (c) 2017-2018 Apple Inc. and the SwiftNIO project authors
// Licensed under Apache License v2.0
//
// See LICENSE.txt for license information
// See CONTRIBUTORS.txt for the list of SwiftNIO project authors
//
// SPDX-License-Identifier: Apache-2.0
//
//===----------------------------------------------------------------------===//
import NIO
import SwiftProtobuf
import Foundation


print("Please enter line to send to the server")
let line = readLine(strippingNewline: true)!

private final class EchoHandler: ChannelInboundHandler {
    public typealias InboundIn = ByteBuffer
    public typealias OutboundOut = ByteBuffer
    private var numBytes = 0
    
	public func CreateHelloWorldRequestJsonString(data: String) -> String {
		
		let jsonString = CreateGenericJsonStrings(identifier: "name", data: data)
		return jsonString
	}
	
	public func CreateLogRequestJsonString(data: String) -> String {
		
		let jsonString = CreateGenericJsonStrings(identifier: "request", data: data)
		return jsonString
	}
	
	public func CreateGenericJsonStrings(identifier: String, data: String) -> String {
		
		let jsonString = "{\"" + identifier + "\":\"" + data + "\"}"
		return jsonString
	}
	
	func evaluateProblem(problemBlock: () -> ()) -> Double
	{
		let start = DispatchTime.now() // Start time
		let myGuess = problemBlock()
		let end = DispatchTime.now()   // end time

		let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
		let timeInterval = Double(nanoTime) / 1_000_000_000

		return timeInterval
	}
	
    public func channelActive(context: ChannelHandlerContext) {
        print("Client connected to \(context.remoteAddress!)")
        
		let jString = CreateHelloWorldRequestJsonString(data: "stopWatchTest")
		
		do
		{
			var total: Double = 0;
			var average: Double = 0;
			for i in 0..<1000 {
				
				let timeEval: Double =
					evaluateProblem(problemBlock: { try! Helloworld_HelloRequest(jsonString: jString)})
				
				total = total + timeEval
				
				let formattedTimeEval = String(format: "%.8f", timeEval)
				print("Test \(i) - \(formattedTimeEval)ms")
				//print("Response \(i) received - \(reply)")
				
			}
			
			average = total / 1000
			let formattedTotal = String(format: "%.8f", total)
			let formattedAverage = String(format: "%.8f", average)
			print("Tests complete. Total - \(formattedTotal) | Average - \(formattedAverage)")
			
		}
		catch
		{
			print("error - \(error)")
		}
		
		do
		{
			//try Helloworld_LogRequest(jsonString )
		}
		
    }

    public func channelRead(context: ChannelHandlerContext, data: NIOAny) {
        var byteBuffer = self.unwrapInboundIn(data)
        self.numBytes -= byteBuffer.readableBytes

        if self.numBytes == 0 {
            if let string = byteBuffer.readString(length: byteBuffer.readableBytes) {
                print("Received: '\(string)' back from the server, closing channel.")
            } else {
                print("Received the line back from the server, closing channel")
            }
            context.close(promise: nil)
        }
    }

    public func errorCaught(context: ChannelHandlerContext, error: Error) {
        print("error: ", error)

        // As we are not really interested getting notified on success or failure we just pass nil as promise to
        // reduce allocations.
        context.close(promise: nil)
    }
}

let group = MultiThreadedEventLoopGroup(numberOfThreads: 1)
let bootstrap = ClientBootstrap(group: group)
    // Enable SO_REUSEADDR.
    .channelOption(ChannelOptions.socket(SocketOptionLevel(SOL_SOCKET), SO_REUSEADDR), value: 1)
    .channelInitializer { channel in
        channel.pipeline.addHandler(EchoHandler())
    }
defer {
    try! group.syncShutdownGracefully()
}

// First argument is the program path
let arguments = CommandLine.arguments
let arg1 = arguments.dropFirst().first
let arg2 = arguments.dropFirst(2).first

let defaultHost = "127.0.0.1" //"::1"
let defaultPort: Int = 50051 //9999

enum ConnectTo {
    case ip(host: String, port: Int)
    case unixDomainSocket(path: String)
}

let connectTarget: ConnectTo
switch (arg1, arg1.flatMap(Int.init), arg2.flatMap(Int.init)) {
case (.some(let h), _ , .some(let p)):
    /* we got two arguments, let's interpret that as host and port */
    connectTarget = .ip(host: h, port: p)
case (.some(let portString), .none, _):
    /* couldn't parse as number, expecting unix domain socket path */
    connectTarget = .unixDomainSocket(path: portString)
case (_, .some(let p), _):
    /* only one argument --> port */
    connectTarget = .ip(host: defaultHost, port: p)
default:
    connectTarget = .ip(host: defaultHost, port: defaultPort)
}

let channel = try { () -> Channel in
    switch connectTarget {
    case .ip(let host, let port):
        return try bootstrap.connect(host: host, port: port).wait()
    case .unixDomainSocket(let path):
        return try bootstrap.connect(unixDomainSocketPath: path).wait()
    }
}()

// Will be closed after we echo-ed back to the server.
try channel.closeFuture.wait()

print("Client closed")
